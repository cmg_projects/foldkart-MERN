import mongoose from 'mongoose';

async function Connection(dbURL) {
    try {
        await mongoose.connect(dbURL);
        console.log("Database connected successfully.")
    } catch (e) {
        console.log("ERR-DB:", e.message);
    }
}

export default Connection;