import path from 'path';
const __dirname = path.resolve();
import express from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import bodyParser from 'body-parser';

import Connection from './database/db.js';
import DefaultData from './default.js';
import Routes from './routes/route.js';

const app = express();
dotenv.config();

app.use(cors());
app.use(express.static(__dirname + '/build'));
app.use(bodyParser.json({ extended: true }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', Routes);

const dbUser = process.env.DB_USERNAME;
const dbPassword = process.env.DB_PASSWORD;
const dbURL = process.env.MONGODB_URL || "mongodb://" + dbUser + ":" + dbPassword + "@ac-bk4t79y-shard-00-00.g1zdfyb.mongodb.net:27017,ac-bk4t79y-shard-00-01.g1zdfyb.mongodb.net:27017,ac-bk4t79y-shard-00-02.g1zdfyb.mongodb.net:27017/?ssl=true&replicaSet=atlas-1482n2-shard-0&authSource=admin&retryWrites=true&w=majority";

Connection(dbURL);

if (process.env.NODE_ENV === 'production') {
    app.use(express.static('build'));
}

app.get('/cmg', (req, res) => {
    res.send('< CMG />');
});

let PORT = process.env.PORT || 8000;
app.listen(PORT, () => console.log("Server is running successfully on PORT " + PORT));

DefaultData();